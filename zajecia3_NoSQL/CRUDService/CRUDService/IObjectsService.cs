﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ObjectsManager.Model;

namespace CRUDService
{
    [ServiceContract]
    public interface IObjectsService
    {
        [OperationContract]
        int AddComics(Comics comics);

        [OperationContract]
        Comics GetComics(int id);

        [OperationContract]
        List<Comics> GetAllComicses();

        [OperationContract]
        Comics UpdateComics(Comics comics);

        [OperationContract]
        bool DeleteComics(int id);


        [OperationContract]
        int AddAuthor(Author author);

        [OperationContract]
        Author GetAuthor(int id);

        [OperationContract]
        List<Author> GetAllAuthors();

        [OperationContract]
        Author UpdateAuthor(Author author);

        //we don't want to serve delete authors method for client (more logic needed for comics authors changes etc.
        //[OperationContract]
        //bool DeleteAuthor(int id);


    }
}
