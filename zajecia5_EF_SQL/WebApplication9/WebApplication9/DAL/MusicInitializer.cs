﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication9.Models;

namespace WebApplication9.DAL
{
    public class MusicInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<MusicContext>
    {
        protected override void Seed(MusicContext context)
        {
            var instruments = new List<Instrument>
            {
                new Instrument() {Name = "Piano"},
                new Instrument() {Name = "Beatbox"},

            };
            instruments.ForEach(i => context.Instruments.Add(i));
            context.SaveChanges();

            var genres = new List<Genre>()
            {
                new Genre() {Name = "Classical"},
                new Genre() {Name="R&B"}
            };
            genres.ForEach(g => context.Genres.Add(g));
            context.SaveChanges();

            var artists = new List<Artist>
            {
                new Artist() {Name = "Kisha", InstrumentID = instruments[0].ID},
                new Artist() {Name = "50Pounds", InstrumentID = instruments[1].ID}
            };
            artists.ForEach(s => context.Artists.Add(s));
            context.SaveChanges();

            var albums = new List<Album>
            {
                new Album() {Title = "4 shades of seasons", GenreID = genres[0].ID, Artists = new List<Artist>(){ artists[0]}},
                new Album() {Title = "RhymeCrime", GenreID = genres[1].ID, Artists = new List<Artist>(){ artists[1]}}
            };
            albums.ForEach(a => context.Albums.Add(a));
            context.SaveChanges();
        }
    }
}