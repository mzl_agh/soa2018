﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication9.Models
{
    public class Artist
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int InstrumentID { get; set; }

        public virtual List<Album> Albums { get; set; }
        public virtual Instrument Instrument { get; set; }
    }

    public class Album
    {
        public int AlbumID { get; set; }
        public string Title { get; set; }
        public int GenreID { get; set; }

        public virtual List<Artist> Artists { get; set; }
        public virtual Genre Genre { get; set; }
    }
    public class Genre
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual List<Album> Albums { get; set; }
    }

    public class Instrument
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Rank { get; set; }
        public string Something { get; set; }
    }
}